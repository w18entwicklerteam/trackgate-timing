tgNamespace.tgContainer.registerInnerGate("timing", function (oTrackGate,sLocalConfData)
{
	this.oTrackGate=oTrackGate;
	this.sLocalConfData=sLocalConfData;
	this.iLog = 0;
	this.oLog = {};

	this.iRegisterLoadedFunction = 2;
	this.iRegisterCountFunction = 0;

	this._construct = function ()
	{
		// Register
		this.oTrackGate.registerInnerGateFunctions("timing","settimepoint",this.setTimepoint);
		this.oTrackGate.registerInnerGateFunctions("timing","final",this.countLoaded);
		this.oTrackGate.registerInnerGateFunctions("timing","pageload",this.countLoaded);
	}

	this.setTimepoint = function(oArgs)
	{
		this.oLog[this.iLog] = oArgs.oTimepoint;
		this.iLog++;
	}

	this.countLoaded = function () {
		this.iRegisterCountFunction++;

		if(this.iRegisterCountFunction == this.iRegisterLoadedFunction)
		{
			this.trackTiming();
		}
	}

	this.trackTiming = function() {
		if(tgNamespace.tgContainer.debug("timing","trackTiming"))debugger;

		if(typeof window.performance !== "undefined") {
			if(typeof window.performance.mark !== "undefined") {
				window.performance.mark("t5_page_loaded");
			}

			var oTiming = window.performance.timing;

			for(var key in oTiming)
			{
				this.oLog[this.iLog] = {
					"system" : "Browser",
					"command" : key,
					"timestamp" : oTiming[key]
				}

				this.iLog++;
			};

			// Berechne Dauer
			for(var key in this.oLog)
			{
				if(this.oLog[key]["timestamp"] == 0 || typeof this.oLog[key]["timestamp"] == "function") {
					this.oLog[key]["timestamp"] = 0;
					this.oLog[key]["duration (s)"] = 0;
				} else {
					this.oLog[key]["duration (s)"] = ((this.oLog[key]["timestamp"] - oTiming.navigationStart) / 1000);
				}
			}

			if(
				this.oTrackGate.bTestMode === true &&
				this.oTrackGate.bLogMode === true
			) {
				console.log("+++++++ TrackGate - Timing +++++++\n\n");
				if(typeof console.table !== "undefined")
				{
                    console.table(this.oLog);
				}
				else
				{
					console.log(this.oLog);
				}

			}

			/* Calculate */
			var dValue = false;
			if(typeof window.performance.measure !== "undefined") {
				window.performance.measure("t5_pageLoadTime","navigationStart","t5_page_loaded");
				var oPageLoadTime = window.performance.getEntriesByName("t5_pageLoadTime");
				dValue = (oPageLoadTime[0].duration) / 1000;
			} else {
				if(typeof oTiming.domComplete !== "undefined" && oTiming.domComplete > new Date((new Date()).valueOf() - 1000*60*60*24).valueOf()) {
					dValue = (oTiming.domComplete - oTiming.navigationStart) / 1000;
				}
			}


			if(dValue !== false) {
				dValue = (Math.round(dValue * 100) / 100);
				if(typeof dValue < 0 || dValue > 100) {
					dValue = false;
				}
			}

			this.oTrackGate.exeFunction({
				track: "timing",
				oTiming: oTiming,
				pageLoadTime: dValue
			});
		}
	}

});

//Config fuer den GTM TrackGate registrieren (diese ist bereits im Einbaucode definiert)
tgNamespace.tgContainer.registerInnerConfig("timing",{
	countConfigs: 1,
	countFuncLibs: 0
}, {});